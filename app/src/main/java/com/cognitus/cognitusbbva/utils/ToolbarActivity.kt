package com.cognitus.cognitusbbva.utils

import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.cognitus.cognitusbbva.databinding.ToolbarBinding

abstract class ToolbarActivity : AppCompatActivity() {

    fun inicializarToolbar(toolbarBinding: ToolbarBinding, titulo: String = "", tipoBtn: Int){
        setSupportActionBar(toolbarBinding.toolbar)
        toolbarBinding.titulo = titulo
        toolbarBinding.toolbar.setNavigationOnClickListener {
            finish()
        }
    }
}