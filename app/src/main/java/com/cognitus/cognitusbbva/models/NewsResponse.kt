package com.cognitus.cognitusbbva.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class NewsResponse (@SerializedName("status") @Expose var status:String,
                    @SerializedName("message")@Expose var images: List<String>){}
