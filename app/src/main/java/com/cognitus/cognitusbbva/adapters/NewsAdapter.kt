package com.cognitus.cognitusbbva.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.cognitus.cognitusbbva.databinding.ItemNewsBinding
import com.cognitus.cognitusbbva.utils.fromUrl

class NewsAdapter (var context: Context, var list: List<String>):
    RecyclerView.Adapter<NewsAdapter.ViewHolder>() {

    override fun getItemCount(): Int = list.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater  = LayoutInflater.from(parent.context)
        val bind = ItemNewsBinding.inflate(inflater,parent,false)
        return ViewHolder(bind)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(list[position])
    }
    inner class ViewHolder(val binding: ItemNewsBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(image: String) {
            binding.ivDog.fromUrl(image)
            binding.executePendingBindings()
        }
    }
}