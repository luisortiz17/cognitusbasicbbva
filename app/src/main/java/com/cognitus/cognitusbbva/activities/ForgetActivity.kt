package com.cognitus.cognitusbbva.activities

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.cognitus.cognitusbbva.R
import com.cognitus.cognitusbbva.databinding.ActivityForgetBinding
import com.cognitus.cognitusbbva.utils.ToolbarActivity


class ForgetActivity : ToolbarActivity() {
    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityForgetBinding>(this, R.layout.activity_forget)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inicializarToolbar(binding.barraP, "Olvide contraseña", 1)

        binding.setClickListener {
            when (it!!.id) {}
        }
    }
}