package com.cognitus.cognitusbbva.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.cognitus.cognitusbbva.R
import com.cognitus.cognitusbbva.utils.SharedPreference

class SplashActivity : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val sharedPreference = SharedPreference(this)
        setContentView(R.layout.activity_splash)
        Handler(Looper.getMainLooper()).postDelayed({
            if (sharedPreference.getValueString("session").isNullOrEmpty()) {
                this.startActivity(Intent(this, MainActivity::class.java))
                this.finish()
            }else{
                this.startActivity(Intent(this, MenuActivity::class.java))
                this.finish()
            }
        }, 3000)
    }
}