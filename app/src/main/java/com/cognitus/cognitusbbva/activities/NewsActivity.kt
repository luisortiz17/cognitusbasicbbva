package com.cognitus.cognitusbbva.activities

import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.cognitus.cognitusbbva.R
import com.cognitus.cognitusbbva.adapters.NewsAdapter
import com.cognitus.cognitusbbva.databinding.ActivityNewsBinding
import com.cognitus.cognitusbbva.models.APIService
import com.cognitus.cognitusbbva.models.NewsResponse
import com.cognitus.cognitusbbva.utils.AlertDialogCustom
import com.cognitus.cognitusbbva.utils.ToolbarActivity
import org.jetbrains.anko.alert
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.jetbrains.anko.yesButton
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class NewsActivity : ToolbarActivity() {
    lateinit var imagesPuppies: List<String>
    lateinit var newsAdapter: NewsAdapter
    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityNewsBinding>(this, R.layout.activity_news)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inicializarToolbar(binding.barraP, "Noticias", 1)
        binding.btnSearch.setOnClickListener {
            searchByName(binding.searchBreed.text.toString().toLowerCase())
        }
    }

    private fun searchByName(query: String) {
        doAsync {
            val call =
                getRetrofit().create(APIService::class.java).getCharacterByName("$query/images")
                    .execute()

            uiThread {
                if (call.isSuccessful) {
                    val puppies = call.body() as NewsResponse
                    if (puppies.status == "success") {
                        initCharacter(puppies)
                    } else {
                        showErrorDialog()
                    }
                } else {
                    AlertDialogCustom.createAlert(this@NewsActivity,"¡Lo sentimos!","No hay datos que coincidan con su búsqueda.")
                }
            }

        }
    }

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://dog.ceo/api/breed/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun initCharacter(puppies: NewsResponse) {
        if (puppies.status == "success") {
            imagesPuppies = puppies.images
        } else {
            alert("Sin datos.") {
                yesButton { }
            }.show()
        }
        newsAdapter = NewsAdapter(this, imagesPuppies)
        binding.rvDogs.setHasFixedSize(true)
        binding.rvDogs.layoutManager = LinearLayoutManager(this)
        binding.rvDogs.adapter = newsAdapter
    }

    private fun showErrorDialog() {
        alert("Ha ocurrido un error, inténtelo de nuevo.") {
            yesButton { }
        }.show()
    }
}