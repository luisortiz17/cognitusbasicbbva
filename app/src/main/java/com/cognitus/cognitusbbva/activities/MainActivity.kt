package com.cognitus.cognitusbbva.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import br.com.ilhasoft.support.validation.Validator
import com.cognitus.cognitusbbva.R
import com.cognitus.cognitusbbva.databinding.ActivityMainBinding
import com.cognitus.cognitusbbva.utils.AlertDialogCustom
import com.cognitus.cognitusbbva.utils.SharedPreference

class MainActivity : AppCompatActivity(), Validator.ValidationListener {
    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val validator: Validator = Validator(binding)
        validator.setValidationListener(this)
        binding.setClickListener {
            when (it!!.id) {
                binding.tvReg.id ->{
                    startActivity(Intent(this, RegisterActivity::class.java))
                }
                binding.tvForget.id ->{
                    startActivity(Intent(this, ForgetActivity::class.java))
                }
                binding.btnLogin.id ->{
                    validator!!.toValidate()
                }
            }
        }
    }
    override fun onValidationSuccess() {
        val sharedPreference = SharedPreference(this)
        sharedPreference.save("session", "1")
        startActivity(Intent(this, MenuActivity::class.java))
    }

    override fun onValidationError() {
        AlertDialogCustom.createAlert(this@MainActivity, "¡Error!","Debé de colocar los datos")
    }
}