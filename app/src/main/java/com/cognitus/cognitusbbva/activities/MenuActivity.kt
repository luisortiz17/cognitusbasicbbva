package com.cognitus.cognitusbbva.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.cognitus.cognitusbbva.R
import com.cognitus.cognitusbbva.databinding.ActivityMenuBinding
import com.cognitus.cognitusbbva.utils.SharedPreference


class MenuActivity : AppCompatActivity() {
    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityMenuBinding>(this, R.layout.activity_menu)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val sharedPreference = SharedPreference(this)
        binding.lyCheck.bringToFront()

        binding.setClickListener {
            when (it!!.id) {
                binding.lyCheck.id -> {
                    startActivity(Intent(this, CheckInActivity::class.java))
                }
                binding.lyProfile.id -> {
                    startActivity(Intent(this, ProfileActivity::class.java))
                }
                binding.lyNotification.id -> {
                    startActivity(Intent(this, NotificationActivity::class.java))
                }
                binding.lyNews.id -> {
                    startActivity(Intent(this, NewsActivity::class.java))
                }
                 binding.lyOff.id -> {
                    sharedPreference.clearSharedPreference()
                     startActivity(Intent(this, MainActivity::class.java))
                     finish()
                }

            }
        }
    }
}