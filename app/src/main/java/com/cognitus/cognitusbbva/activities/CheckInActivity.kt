package com.cognitus.cognitusbbva.activities

import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.media.MediaScannerConnection
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.cognitus.cognitusbbva.R
import com.cognitus.cognitusbbva.databinding.ActivityCheckInBinding
import com.cognitus.cognitusbbva.utils.ToolbarActivity
import com.github.gcacace.signaturepad.views.SignaturePad
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*

class CheckInActivity : ToolbarActivity() {
    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityCheckInBinding>(this, R.layout.activity_check_in)
    }
    companion object {

        val TAG = "PermissionDemo"
        private const val REQUEST_STORAGE = 200
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inicializarToolbar(binding.barraP, "Check In", 1)
        revisaPermiso()

        binding.etTime.isEnabled=false
        binding.setClickListener {
            when (it!!.id) {
                binding.btnSave.id -> {
                    val signatureBitmap: Bitmap = binding.signaturePad.transparentSignatureBitmap
                    if (!saveImage(signatureBitmap).isNullOrEmpty()) {
                        Toast.makeText(this, "Firma guardada", Toast.LENGTH_SHORT).show()
                        binding.signaturePad.clear()
                    } else {
                        Toast.makeText(
                            this,
                            "Unable to store the signature",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
                binding.btnClear.id -> {
                    binding.signaturePad.clear()
                }
                binding.etHora.id->{
                    showTimePickerDialog()
                }
            }
        }

        binding.signaturePad.setOnSignedListener(object : SignaturePad.OnSignedListener {
            override fun onStartSigning() { //Toast.makeText(SignActivity.this, "OnStartSigning", Toast.LENGTH_SHORT).show();
            }

            override fun onSigned() {
                binding.btnSave.isEnabled = true
                binding.btnClear.isEnabled = true
            }

            override fun onClear() {
                binding.btnSave.isEnabled = false
                binding.btnClear.isEnabled = false
            }
        })



    }
    private fun showTimePickerDialog() {
        val timePicker = TimePickerFragment { onTimeSelected(it) }
        timePicker.show(supportFragmentManager, "timePicker")
    }
    private fun onTimeSelected(time: String) {
        binding.etTime.text = "$time"
    }
    fun revisaPermiso(){
        if (ContextCompat.checkSelfPermission(this,android.Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE),
                REQUEST_STORAGE
            )
            Log.i(TAG, "Pide permiso")
        }

    }
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_STORAGE -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.i(TAG, "Si dio permiso")
            }else{
                Log.i(TAG, "No dio permiso")
            }
        }
    }

    fun saveImage(myBitmap: Bitmap): String {
        val bytes = ByteArrayOutputStream()
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes)
        val wallpaperDirectory = File(
            (Environment.getExternalStorageDirectory()).toString() + "/cognitus"
        )
        // build the directory structure
        Log.d("file", wallpaperDirectory.toString())
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs()
        }
        try {
            val f = File(
                wallpaperDirectory, ((Calendar.getInstance()
                    .getTimeInMillis()).toString() + ".jpg")
            )
            f.createNewFile()
            val fo = FileOutputStream(f)
            fo.write(bytes.toByteArray())
            MediaScannerConnection.scanFile(
                this,
                arrayOf(f.getPath()),
                arrayOf("image/jpeg"), null
            )
            fo.close()
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath())

            return f.getAbsolutePath()
        } catch (e1: IOException) {
            e1.printStackTrace()
        }
        return ""
    }
}