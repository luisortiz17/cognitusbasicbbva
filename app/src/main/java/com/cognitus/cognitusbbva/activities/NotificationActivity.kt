package com.cognitus.cognitusbbva.activities


import android.os.Bundle
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.cognitus.cognitusbbva.R
import com.cognitus.cognitusbbva.adapters.NotificationAdapter
import com.cognitus.cognitusbbva.databinding.ActivityNotificationBinding
import com.cognitus.cognitusbbva.models.NotificationModel
import com.cognitus.cognitusbbva.utils.ToolbarActivity


class NotificationActivity : ToolbarActivity() {
    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityNotificationBinding>(this, R.layout.activity_notification)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inicializarToolbar(binding.barraP, "Notificaciones", 1)

        //adding a layoutmanager
        binding.rvNotification.layoutManager = LinearLayoutManager(this)

        //crating an array list to store users using the data class user
        val notifications = ArrayList<NotificationModel>()

        //adding some dummy data to the list
        notifications.add(NotificationModel("Título de Notificación 1", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vulputate libero viverra ligula laoreet semper.","R"))
        notifications.add(NotificationModel("Título de Notificación 2", "Donec vulputate libero viverra ligula laoreet semper.Lorem ipsum dolor sit amet, consectetur adipiscing elit. ","R"))
        notifications.add(NotificationModel("Título de Notificación 3", "Vestibulum arcu orci, hendrerit ac odio et, rhoncus aliquet lacus. Donec a nisl auctor, elementum urna sed.","L"))
        notifications.add(NotificationModel("Título de Notificación 4", "Phasellus non arcu vitae turpis pharetra pellentesque. Sed id auctor nisl, vel sollicitudin nisl.","L"))

        //creating our adapter
        val adapter = NotificationAdapter(this,notifications)

        //adding the adapter to recyclerview
        binding.rvNotification.adapter = adapter
    }
}