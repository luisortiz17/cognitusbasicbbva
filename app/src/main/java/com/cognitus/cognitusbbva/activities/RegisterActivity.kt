package com.cognitus.cognitusbbva.activities

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.cognitus.cognitusbbva.R
import com.cognitus.cognitusbbva.databinding.ActivityRegisterBinding
import com.cognitus.cognitusbbva.utils.ToolbarActivity


class RegisterActivity : ToolbarActivity() {
    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityRegisterBinding>(this, R.layout.activity_register)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inicializarToolbar(binding.barraP, "Registro", 1)

        binding.setClickListener {
            when (it!!.id) {}
        }
    }
}